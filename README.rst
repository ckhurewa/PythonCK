========
PythonCK
========

.. image:: https://img.shields.io/pypi/v/pythonck.svg
   :target: https://pypi.python.org/pypi/pythonck
.. image:: https://gitlab.com/ckhurewa/pythonck/badges/master/pipeline.svg
   :target: https://gitlab.com/ckhurewa/pythonck/commits/master
.. image:: https://gitlab.com/ckhurewa/pythonck/badges/master/coverage.svg
   :target: https://ckhurewa.gitlab.io/pythonck
.. image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
.. image:: https://readthedocs.org/projects/pythonck/badge/?version=latest
   :target: http://pythonck.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status
.. image:: https://img.shields.io/pypi/pyversions/pythonck.svg


Collection of utilities & helper functions,
mainly used during my PhD at EPFL/CERN.

Features:

- Lots of helpful decorators: caching, report, singleton, etc.
- io/string/iterable utilities that are commonly encountered.
- Logger adapted to my taste.
